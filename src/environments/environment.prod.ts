export const environment = {
  production: true,
  splashScreenDuration: 1500,
  minParseDuration: 1000
};
