import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { environment } from '../environments/environment';
import { reducers } from './store/reducers';
import { AppComponent } from './app.component';
import { MapComponent } from './components/map/map.component';
import { FileDropOverlayComponent } from './components/file-drop-overlay/file-drop-overlay.component';
import { DragDropDirective } from './directives/drag-drop.directive';
import { LoadingLayerComponent } from './components/loading-layer/loading-layer.component';
import { SplashScreenComponent } from './components/splash-screen/splash-screen.component';
import { LogScreenComponent } from './components/log-screen/log-screen.component';
import { AppEffects } from './store/app.effects';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    FileDropOverlayComponent,
    DragDropDirective,
    LoadingLayerComponent,
    SplashScreenComponent,
    LogScreenComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot(reducers, {
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([AppEffects])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
