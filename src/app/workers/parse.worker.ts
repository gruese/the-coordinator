/// <reference lib="webworker" />

import { AppMessage, AppMessageType, ParseResult, System } from '../types';
import { readFileAsUint8Array } from '../utils';
import * as xlsx from 'xlsx';

addEventListener('message', async ({ data }) => {
  const response: ParseResult = {
    success: true,
    messages: [],
    parsedData: []
  };
  try {
    const { parsedData, messages } = await parseFile(data as File);
    response.parsedData = parsedData;
    response.messages = messages;
  } catch (e) {
    response.success = false;
    response.messages.push({
      type: AppMessageType.Error,
      message: `Parsing failed: ${e.message}`
    });
  }
  postMessage(response);
});

async function parseFile(file: File) {
  console.log('now attempting to convert file', file.name);
  // shortcut to encode_cell function
  const getCell = (sheet: xlsx.WorkSheet, c: number, r: number) => sheet[xlsx.utils.encode_cell({ c, r })];

  const messages: AppMessage[] = [];
  const fileByteArray = await readFileAsUint8Array(file);
  const parsedData: System[] = [];
  const workbook = xlsx.read(fileByteArray, { type: 'array', WTF: true });
  if (!workbook?.Props?.Application) {
    throw new Error('Wrong file type: This does not seem to be a valid spreadsheet');
  }
  if (workbook.SheetNames.length === 0) {
    throw new Error('This workbook does not have any sheets');
  }
  const sheet = workbook.Sheets[workbook.SheetNames[0]];
  const sheetRange = xlsx.utils.decode_range(sheet['!ref']);
  for (let row = 1; row <= sheetRange.e.r; row++) {
    const nameCell = getCell(sheet, 0, row);
    const xCell = getCell(sheet, 1, row);
    const yCell = getCell(sheet, 2, row);
    if (!nameCell?.w) {
      continue;
    }
    const systemName = nameCell.w.trim();
    const xCoordinate = parseFloat(xCell.v);
    if (isNaN(xCoordinate)) {
      console.error(xCell.v);
      messages.push({
        type: AppMessageType.Warning,
        message: `X Coordinate for system "${systemName}" cannot be parsed as a number`
      });
      continue;
    }
    const yCoordinate = parseFloat(yCell.v);
    if (isNaN(yCoordinate)) {
      console.error(yCell.v);
      messages.push({
        type: AppMessageType.Warning,
        message: `Y Coordinate for system "${systemName}" cannot be parsed as a number`
      });
      continue;
    }

    // console.log('SYSTEM', systemName, xCoordinate, yCoordinate);
    parsedData.push({
      name: systemName,
      x: xCoordinate,
      y: yCoordinate
    });
  }
  return {
    parsedData,
    messages
  };
}
