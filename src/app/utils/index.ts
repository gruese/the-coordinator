import { Point2d } from '../types';

export function readFileAsUint8Array(file: File): Promise<Uint8Array> {
  return new Promise((resolve, reject) => {
    const fileReader = new FileReader();
    fileReader.onerror = (e) => {
      console.error('error while reading file buffer', e);
      // eslint-disable-next-line prefer-promise-reject-errors
      reject((e?.target as FileReader)?.error);
    };
    fileReader.onload = () => resolve(new Uint8Array(fileReader.result as ArrayBuffer));
    fileReader.readAsArrayBuffer(file);
  });
}

/**
 * Returns euclidean distance between two 2d points
 * @param a The first point
 * @param b The second point
 */
export function calcDistance2d(a: Point2d, b: Point2d): number {
  return Math.sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
}

export function sleep(ms = 0): Promise<void> {
  return new Promise(resolve => setTimeout(resolve, ms));
}
