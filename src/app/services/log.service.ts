import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';
import { addLogMessage } from '../store/actions';
import { AppMessageType } from '../types';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  constructor(private store: Store<AppState>) {}

  private addMessage(type: AppMessageType, args: any[]) {
    args.forEach(arg =>
      this.store.dispatch(addLogMessage({
        message: {
          type: type,
          message: String(arg)
        }
      }))
    );
  }

  log(...args: any[]) {
    console.log(...args);
    this.addMessage(AppMessageType.Log, args || []);
  }

  warning(...args: any[]) {
    console.warn(...args);
    this.addMessage(AppMessageType.Warning, args || []);
  }

  error(...args: any[]) {
    console.error(...args);
    this.addMessage(AppMessageType.Error, args || []);
  }
}
