import { Injectable } from '@angular/core';
import { combineLatest, fromEvent, of } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { ParseResult } from '../types';

@Injectable({
  providedIn: 'root'
})
export class ParseService {
  private parseWorker?: Worker;

  constructor() {
    this.initParseWorker();
  }

  initParseWorker() {
    if (typeof Worker !== 'undefined') {
      this.parseWorker = new Worker('../workers/parse.worker', { type: 'module' });
    } else {
      // TODO deal with scenarios where web workers are not supported
    }
  }

  parseFile(file: File) {
    console.log('parseFile called with', file);
    if (!this.parseWorker) {
      throw new Error('Could not parse file: Parse worker was not initialized.');
    }
    const workerMessage$ = combineLatest([
      fromEvent<MessageEvent>(this.parseWorker, 'message'),
      of(1).pipe(delay(environment.minParseDuration))
    ]).pipe(
      map(([message]) => message.data as ParseResult)
    );
    this.parseWorker.postMessage(file);
    return workerMessage$;
  }
}
