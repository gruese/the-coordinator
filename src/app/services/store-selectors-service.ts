import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';
import { selectActiveLayer } from '../store/selectors';

@Injectable({
  providedIn: 'root'
})
export class StoreSelectorsService {
  constructor(private store: Store<AppState>) {}

  activeLayer$ = this.store.select(selectActiveLayer);
}
