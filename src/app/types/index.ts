export enum LayerId {
  None = 'None',
  Splash = 'Splash',
  FileDrop = 'FileDrop',
  Loading = 'Loading',
  Messages = 'Messages',
  Map = 'Map'
}

export enum AppMessageType {
  Log = 'Log',
  Warning = 'Warning',
  Error = 'Error'
}

export interface AppMessage {
  message: string,
  type: AppMessageType
}

export interface ParseResult {
  messages: AppMessage[];
  success: boolean;
  parsedData?: System[];
}

export interface Point2d {
  x: number;
  y: number;
}

export interface System extends Point2d {
  name: string;
}
