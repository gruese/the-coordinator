import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { timer } from 'rxjs';
import { StoreSelectorsService } from './services/store-selectors-service';
import { LayerId } from './types';
import { AppState } from './store/types';
import { setActiveLayer, setSourceFile } from './store/actions';
import { environment } from '../environments/environment';
import { LogService } from './services/log.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  LayerId = LayerId;
  title = 'interactive-point-map';

  constructor(
    private store: Store<AppState>,
    public selectorsService: StoreSelectorsService,
    private logService: LogService
  ) {
  }

  ngOnInit() {
    // leave splash screen for configured time
    // const splashSubscription = timer(environment.splashScreenDuration).subscribe(() => {
    //   this.store.dispatch(setActiveLayer({ active: LayerId.FileDrop }));
    //   splashSubscription.unsubscribe();
    // });
  }

  onFileDropErrorThrown(e: Error) {
    this.logService.error(e);
  }

  onFileDropped(file: File) {
    this.store.dispatch(setActiveLayer({ active: LayerId.Loading }));
    this.store.dispatch(setSourceFile({ file }));
  }
}
