import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-file-drop-overlay',
  templateUrl: './file-drop-overlay.component.html',
  styleUrls: ['./file-drop-overlay.component.scss']
})
export class FileDropOverlayComponent implements OnInit {
  @ViewChild('fileInputRef', { static: false }) fileInputEl: ElementRef;
  @Output() fileUploaded: EventEmitter<File> = new EventEmitter();
  @Output() errorThrown: EventEmitter<Error> = new EventEmitter();
  canDropUpload = false;

  ngOnInit(): void {
    this.canDropUpload = this.featureDetectDropUpload();
  }

  featureDetectDropUpload(): boolean {
    const div = document.createElement('div');
    return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
  }

  onFileDropped(files: FileList): void {
    this.evaluateFileList(files);
  }

  onFileInputChange(el: EventTarget): void {
    this.evaluateFileList((el as HTMLInputElement).files);
  }

  evaluateFileList(files: FileList): void {
    if (!files || files.length === 0) {
      this.errorThrown.emit(new Error('No file uploaded'));
      return;
    } else if (files.length > 1) {
      this.errorThrown.emit(new Error('Only single files can be uploaded'));
      return;
    }
    this.fileUploaded.emit(files[0]);
    this.fileInputEl.nativeElement.value = '';
  }
}
