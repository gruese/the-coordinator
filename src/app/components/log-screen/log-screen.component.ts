import { Component, OnInit } from '@angular/core';

enum MessageType {
  Error = 'Error',
  Warning = 'Warning',
  Log = 'Log'
}

interface LogMessage {
  type: MessageType;
  messageText: string;
}

@Component({
  selector: 'app-log-screen',
  templateUrl: './log-screen.component.html',
  styleUrls: ['./log-screen.component.scss']
})
export class LogScreenComponent implements OnInit {
  logMessages: LogMessage[] = [];

  ngOnInit(): void {
    // init
    // this.logMessages.push({ type: MessageType.Error, messageText: 'Test 1' });
    // this.logMessages.push({ type: MessageType.Warning, messageText: 'Test 2' });
    // this.logMessages.push({ type: MessageType.Log, messageText: 'Test 3' });
    // console.log(this.logMessages);
  }
}
