import { Action, ActionReducerMap, createReducer, on } from '@ngrx/store';
import { InitialState } from './initial-state';
import {
  addLogMessage,
  addLogMessages,
  clearLogMessages,
  clearSourceFile,
  parseFailure,
  parseSuccess, resetApplication,
  setActiveLayer,
  setSourceFile
} from './actions';
import { AppState } from './types';
import { AppMessage, AppMessageType, LayerId, System } from '../types';
import { environment } from '../../environments/environment';

function outputLogMessages(messages: AppMessage[]) {
  if (!environment.production) {
    messages.forEach(message => {
      if (!message) {
        return;
      }
      switch (message.type) {
        case AppMessageType.Error:
          console.error('(FROM REDUCER)', message.message);
          break;
        case AppMessageType.Warning:
          console.warn('(FROM REDUCER)', message.message);
          break;
        default:
          console.log('(FROM REDUCER)', message.message);
      }
    });
  }
}

export function activeLayer(state: LayerId, action: Action): LayerId {
  return createReducer(InitialState.activeLayer,
    on(setActiveLayer, (state, { active }) => active),
    on(parseSuccess, () => LayerId.Map),
    on(parseFailure, () => LayerId.Messages)
  )(state, action);
}

export function messages(state: AppMessage[], action: Action): AppMessage[] {
  return createReducer(InitialState.messages,
    on(addLogMessages, (state, { messages }) => {
      outputLogMessages(messages);
      return [...state, ...messages];
    }),
    on(addLogMessage, (state, { message }) => {
      outputLogMessages([message]);
      return [...state, message];
    }),
    on(clearLogMessages, () => [])
  )(state, action);
}

export function sourceFile(state: File|undefined, action: Action): File|undefined {
  return createReducer(InitialState.sourceFile,
    on(resetApplication, () => undefined),
    on(setSourceFile, (state, { file }) => file),
    on(clearSourceFile, () => undefined)
  )(state, action);
}

export function parsedSystems(state: System[]|undefined, action: Action): System[]|undefined {
  return createReducer(InitialState.parsedSystems,
    on(resetApplication, () => undefined),
    on(parseFailure, () => undefined),
    on(parseSuccess, (state, { result }) => result.parsedData)
  )(state, action);
}

export const reducers: ActionReducerMap<AppState> = {
  activeLayer,
  messages,
  sourceFile,
  parsedSystems
};
