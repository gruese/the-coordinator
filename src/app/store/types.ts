import { AppMessage, LayerId, System } from '../types';

export interface AppState {
  activeLayer: LayerId;
  sourceFile?: File;
  parsedSystems?: System[];
  messages: AppMessage[];
}
