// import { createSelector } from '@ngrx/store';
import { AppState } from './types';

export const selectActiveLayer = (state: AppState) => state.activeLayer;
export const selectParsedSystems = (state: AppState) => state.parsedSystems;
