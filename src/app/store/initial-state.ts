import { AppState } from './types';
import { LayerId } from '../types';

export const InitialState: AppState = {
  // activeLayer: LayerId.Map,
  activeLayer: LayerId.FileDrop,
  messages: []
};
