import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { ActionType } from './actions';
import { ParseService } from '../services/parse.service';
import { AppMessage, AppMessageType, ParseResult } from '../types';
import { EMPTY, of } from 'rxjs';

@Injectable()
export class AppEffects {
  constructor(
    private actions$: Actions,
    private parseService: ParseService
  ) {}

  parseSourceFile$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActionType.SetSourceFile),
      mergeMap((action: { file: File|undefined }) => {
        if (!action.file) {
          return EMPTY;
        }
        try {
          return this.parseService.parseFile(action.file).pipe(
            map(parseFileResult =>
              ({ type: ActionType.ParseSuccess, result: parseFileResult })),
            catchError((e) =>
              of({ type: ActionType.ParseFailure, error: e }))
          );
        } catch (e) {
          return of({ type: ActionType.ParseFailure, error: e });
        }
      })
    )
  );

  reportParseSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActionType.ParseSuccess),
      mergeMap((action: { result: ParseResult }) => {
        return of({
          type: ActionType.AddLogMessages,
          messages: action.result.messages
        });
      })
    )
  );

  reportParseFailure$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActionType.ParseFailure),
      mergeMap((action: { result?: ParseResult, error?: Error }) => {
        const messages: AppMessage[] = [];
        if (action.result) {
          messages.push(...action.result.messages);
        } else if (action.error) {
          messages.push({ type: AppMessageType.Error, message: action.error.message });
        } else {
          messages.push({ type: AppMessageType.Error, message: 'Parse failure' });
        }
        return of({
          type: ActionType.AddLogMessages,
          messages
        });
      })
    )
  );
}
