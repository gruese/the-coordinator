import { createAction, props } from '@ngrx/store';
import { AppMessage, LayerId, ParseResult } from '../types';

export enum ActionType {
  ResetApplication = '[Application State] Reset application',
  AddLogMessages = '[App Logs] Add list of log messages',
  AddLogMessage = '[App Logs] Add log message',
  ClearAllLogs = '[App Logs] Clear all log messages',
  SetActiveLayer = '[App Layout] Set active layer',
  SetSourceFile = '[Source File] Set source file',
  ClearSourceFile = '[Source File] Clear source file',
  ParseSuccess = '[File Parse] Parse success',
  ParseFailure = '[File Parse] Parse failure'
}

export const resetApplication = createAction(
  ActionType.ResetApplication
);

export const addLogMessages = createAction(
  ActionType.AddLogMessages,
  props<{ messages: AppMessage[] }>()
);

export const addLogMessage = createAction(
  ActionType.AddLogMessage,
  props<{ message: AppMessage }>()
);

export const clearLogMessages = createAction(
  ActionType.ClearAllLogs
);

export const setActiveLayer = createAction(
  ActionType.SetActiveLayer,
  props<{ active: LayerId }>()
);

export const setSourceFile = createAction(
  ActionType.SetSourceFile,
  props<{ file: File }>()
);

export const clearSourceFile = createAction(
  ActionType.ClearSourceFile
);

export const parseSuccess = createAction(
  ActionType.ParseSuccess,
  props<{ result: ParseResult }>()
);

export const parseFailure = createAction(
  ActionType.ParseFailure,
  props<{ result?: ParseResult, error?: Error }>()
);
